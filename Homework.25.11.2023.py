# 1. Write a Python script to sort (ascending and descending) a dictionary by value.

def sort_by_values(dict1):
    
    keys=list(dict1.keys())
    values=list(dict1.values())
    
    dict1={k:v for k, v in zip(keys, sorted(values, reverse=False))}
    print("The dictionary sorted by values ascending",dict1)
    
    dict1={k:v for k, v in zip(keys, sorted(values, reverse=True))}
    print("The dictionary sorted by values descending",dict1)
dict1={"a":15, "d":0,"b":-2}
sort_by_values(dict1)

# 2. Access dictionary key’s element by index.

dict2 = {'a': 23, 'c': 32, 'm': 7, 'v': 19,  'k': 22}
key = list(dict2)
print("The dictionay key by index 0: ",key[0])

dict2 = {'a': 23, 'c': 32, 'm': 7, 'v': 19,  'k': 22}  
key = [k for k in key]                             # by list comprehension
print("The dictionay key by index 3: ",key[3])

# 3. Drop empty Items from a given Dictionary.

dict3 = {'a': None, 'c': 32, 'm': 7, 'v': 19,  'k': None} 
dict3 = {k:v  for k,v in dict3.items() if v!=None}                           
print(dict3)

# 4. Write a Python program to print the given dictionary line by line.

Di = {  "Sam" : {"M1" : 89, "M2" : 56, "M3" : 89},
               "Suresh" : {"M1" : 49, "M2" : 96, "M3" : 89} }
for i in Di:
    print(i)
    for j in Di[i]:
        print(j,":",Di[i][j])

# 5. Write a Python program to add member(s) in a set

set1={"Lion", "Cat"}
set1.add("Dog") 
print("Add single element 'Dog':",set1)
list1=["Panda", "Tiger"]
set1.update(list1)
print("Add multiple items ['Panda', 'Tiger'] :", set1)

# 6. Write a Python program to create an intersection of sets.

def set_intersection(set2, set3):
    set4=set()
    for i in set2:
        if i in set3:
            set4.add(i)
    return set4   
set2={"a","b","d"}
set3={"d","t","s"}
print(f"The intersection of {set2} and {set3}: ",set_intersection(set2, set3))    

# 7. Write a Python program to create a union of sets.
x = {"apple", "banana", "cherry"}
y = {"google", "microsoft", "apple"}
z = x.union(y)              # Version 1  
x.update(y)                 # Version 2
print("The union of sets",z)
print("The union of sets",x)

new_set=set()               # Version 3
for i in x:
    for g in y:
        new_set.add(i)
        new_set.add(g)
print("The union of sets",new_set) 

# 8. Write a Python program to create set difference.
set4 = {"a", "b", "c"}
set5= {"g", "m", "a"}
set45 = set4.difference(set5) 
print("The difference between two sets",set45)

n_set=set()
for i in set4:
    if i not in set5:
        n_set.add(i)
print("The difference between two sets",n_set)  

# 9. Write a Python program to create a symmetric difference .
set6 = {"a", "b", "c"}
set7= {"g", "m", "a"}
set67 = set6.symmetric_difference(set7) 
print("The symmetric_difference between two sets",set67)

new_set1=set()
for i in set6:
    for g in set7:
        if i in set7 and i in set6:
            continue
        if g in set7 and g in set6:
            continue
        else:
            new_set1.add(i)
            new_set1.add(g)
print("The symmetric_difference between two sets",new_set1)   

# 10. Write a Python program to Check if a set is a subset, using comparison operators.
set8 = {"a", "b", "c"}
set9 = {"f", "e", "d", "c", "b", "a"}
new_set2= set8.issubset(set9) 

print("Is set8 subset ob set9: ",new_set2)
print("Is set8 subset ob set9: ",set9>=set8)
print("Is set9 subset ob set8: ",set8>=set9)
        
# 11. Word guessing game //In this game, there is a list of words present, out of which our interpreter will choose
    #1 random word. The user first has to input their names and then, will be asked to guess any character.
    # If the random word contains that character, it will be shown as the output(with correct placement) 
    # else the program will ask you to guess another character. The user will be given 12 turns(which can be changed accordingly) to guess the complete word.



# 12. Develop a program that  translates text into Morse code. 
def text_to_Morse(text):

    dict1={"a":".- ", "b": "-... ","c": "-.-. ", "d": "-.. ", "e": ". ", "f": "..-. ", "g": "--. ", "h": ".... ","i": ".. ",
        "j": ".--- ", "k": "-.- ", "l": ".-.. ", "m": "-- ", "n": "-. ", "o": "--- ", "p": ".--. ", "q": "--.- ",
        "r": ".-. ", "s": "... ", "t": "- ", "u": "..- ", "v": "...- ", "w": ".-- ", "x": "-..- ", "y": "-.-- ","z": "--.. "
    }   
    list1=[dict1[i] for i in inp] 
    str1=''.join(i for i in list1)
    return str1    
    
inp=input("Please enter a text to convert it to Morse code: ")
Morse_code=text_to_Morse(inp)
print(Morse_code)    
